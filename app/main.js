var app = angular.module('dvrHelper', ['ui.router', 'ui.bootstrap', 'icon-awesome', 'uiSwitch', 'ngSanitize']);
var gui = require('nw.gui');

global.$ = $;
var dvrHelper = require('dvr-helper');

dvrHelper.init(app, gui);
